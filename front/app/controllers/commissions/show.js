import Controller from '@ember/controller';

export default Controller.extend({
	sorted: false,


	sortedItems: Ember.computed('model', function () {
		// body...
		return this.get('model').get('members').sortBy('legislator.block.name');
	}),

	actions: {
		toggleSort: function () {
			this.toggleProperty('sorted');
		}
	}
});
