import Controller from '@ember/controller';

export default Controller.extend({

	blockChanged: Ember.observer('selectedBlock', function () {
		if (this.get('selectedBlock')) {
			this.set('model', this.store.query('legislator', {block: this.get('selectedBlock').get('id')}))
		} else {
			this.set('model', this.store.findAll('legislator'))
		}
	})
});
