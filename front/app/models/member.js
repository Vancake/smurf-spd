import DS from 'ember-data';

export default DS.Model.extend({
	legislator: DS.belongsTo('legislator'),
	commission: DS.belongsTo('commission'),
	charge: DS.belongsTo('charge')
});
