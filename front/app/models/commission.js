import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	createdAt: DS.attr('string'),
	updatedAt: DS.attr('string'),
	members: DS.hasMany('member', { async: true})
});
