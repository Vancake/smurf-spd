import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	last_name: DS.attr('string'),
	photo: DS.attr('string'),
	email: DS.attr('string'),
	study: DS.attr('string'),
	sex: DS.attr('string'),
	charge: DS.attr('string'),
	block: DS.belongsTo('block')
});
