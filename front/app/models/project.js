import DS from 'ember-data';

export default DS.Model.extend({
  	type: DS.attr('string'),
	title: DS.attr('string'),
	expdip: DS.attr('string'),
	iniciated: DS.attr('string'),
	mje_num: DS.attr('string'),
	mje_date: DS.attr('string'),
	
	legislators: DS.hasMany('legislator', { async: true }),
	commissions: DS.hasMany('commission', { async: true }),
});
