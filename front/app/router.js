import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
	this.route("commissions", function() {
	  this.route("show", {
	    path: ":commission_id"
	  });
	});  
	this.route("projects", function() {
	  this.route("show", {
	    path: ":project_id"
	  });
	});  	
	this.route("legislators", function() {
	  this.route("show", {
	    path: ":legislator_id"
	  });
	});  	
});

export default Router;
