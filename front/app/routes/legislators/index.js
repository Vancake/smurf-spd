import Route from '@ember/routing/route';

export default Route.extend({
	selectedBlock: null,

	model: function () {
		return this.store.findAll('legislator');
	},

	setupController: function (controller, model) {
		this._super(controller, model);
		controller.set('blocks', this.store.findAll('block'));
	},
});
