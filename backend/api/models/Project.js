/**
 * Project.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	type: 'string',
	title: 'string',
	expdip: 'string',
	iniciated: 'string',
	mje_num: 'string',
	mje_date: 'string',
	legislators: 'array',
	commissions: 'array',
	commision: 'string',
	legislator: 'string',

  }
};

