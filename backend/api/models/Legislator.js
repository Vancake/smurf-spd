/**
 * Legislator.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
  	charge: 'string',
  	name: 'string',
  	last_name: 'string',
  	full_name: 'string',
  	sex: 'string',
  	email: 'string',
  	study: 'string',
  	photo: 'string',

  	block: {
  		model: 'block'
  	}
  }
};

