/**
 * DataController
 *
 * @description :: Server-side logic for managing data
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs = require('fs');

module.exports = {

	loadProjects: function (req, res) {
		var obj;
		fs.readFile('data/projects.json', 'utf8', function (err, data) {
		  if (err) throw err;
		  
		  var projects = [];

		  obj = JSON.parse(data);


		  Commission.find().exec(function (err, commissions) {
		  	Legislator.find().exec(function (err, legislators) {
				  obj.proyectos.forEach(function (p) {
				  	var project = {};
				  	project.id = p.id;
				  	project.title = p.titulo;
				  	project.type = p.tipo;
				  	project.expdip = p.expdip;
				  	project.iniciated = p.iniciado;
				  	project.mje_num = p.mjeNum;
				  	project.mje_date = p.mjeFecha;
				  	project.commissions = [];
				  	project.legislators = [];

				  	var i = 0;
				  	p.giro.forEach(function (giro) {
				  		var c = commissions.find(o => o.name === giro.comision);
				  		if (c) {
				  			project.commissions.push(c.id);
				  			if (i === 0) project.commission = c.id;
				  			i++;
				  		}
				  	});

				  	var i = 0;
				  	p.firmantes.forEach(function (firmante) {
				  		var name = firmante.nombre.split(',')[1].trim();
				  		var last_name = firmante.nombre.split(',')[0].trim();
				  		var c = legislators.find(o => o.name === name && o.last_name === last_name);
				  		if (c) {
				  			project.legislators.push(c.id);
				  			if (i === 0) project.legislator = c.id;
				  			i++;
				  		}
				  	});				  	

				  	if (!project.legislators[0])
				  		delete project.legislators;

				  	projects.push(project);
				  });

				  Project.findOrCreate(projects).exec(function (err, projectsCreated) {
				  	res.ok(projectsCreated);
				  })
		  	});
		  });
		});		
	},

	loadDates: function (req, res) {
		var obj;
		fs.readFile('data/dates.json', 'utf8', function (err, data) {
		  if (err) throw err;
		  obj = JSON.parse(data);
		  var citations = [];

		  data.forEach(function (c) {
		  	var citation = {};
		  	citation.id = c.id;
		    citation.summary = c.resumen;
		    citation.saloon = c.sala.numero;
		    citation.state = c.estado.descripcion;
		    citation.observations = c.observaciones;
		    citation.title = c.title;
		    citation.start = c.start;
		    citation.commissions = [];

		    c.comisiones.forEach(function (cc) {
		    	citation.commissions.push(cc.id);
		    });

		    if (!citation.commissions[0])
		    	delete citation.commissions;

		  })


		  res.ok(obj);
		});		
	},

	loadCommissions: function (req, res) {
		var obj;
		fs.readFile('data/commissions.json', 'utf8', function (err, data) {
		  if (err) throw err;
		  obj = JSON.parse(data);
		  console.log(obj);

		  res.ok(obj);
		});		
	},

	loadLegislators: function (req, res) {
		var obj;
		fs.readFile('data/commissions.json', 'utf8', function (err, data) {
		  if (err) throw err;
		  obj = JSON.parse(data);
 		  var legislators = [];
 		  var blocks = [];
 		  var commissions = [];
 		  var charges = [];
 		  var members = [];

 		  obj.forEach(function (commission) {
 		  	commissions.push({
 		  		name: commission.nombre,
 		  		id: commission.id
 		  	});

 		  	commission.integrantes.forEach(function (integrante) {
 		  		var lastBlock = integrante.diputado.datosPersonales.bloques.find(o => new Date(o.fechaFin) > new Date());

 		  		var block = blocks.find(o => o.name === lastBlock.nombre);

 		  		if (!block) {
 		  			blocks.push({
 		  				name: lastBlock.nombre,
 		  				id: lastBlock.id
 		  			});
 		  			block = lastBlock;
 		  		}

 		  		var charge = charges.find(o => o.id === integrante.cargo.id);
 		  		if (!charge)
 		  			charges.push({
 		  				id: integrante.cargo.id,
 		  				name: integrante.cargo.descripcion
 		  			});		

 		  		var picked = legislators.find(o => o.id === integrante.diputado.id);
 		  		if (!picked) {
 		  			var diputado = {};
 		  			diputado.id = integrante.diputado.id;
 		  			diputado.name = integrante.diputado.datosPersonales.nombre;
 		  			diputado.charge = integrante.diputado.cargo;
 		  			diputado.last_name = integrante.diputado.datosPersonales.apellido;
 		  			diputado.block = block.id;
 		  			diputado.email = integrante.diputado.datosPersonales.email;
 		  			diputado.study = integrante.diputado.datosPersonales.despacho;
 		  			diputado.sex = integrante.diputado.datosPersonales.sexo;
 		  			diputado.photo = 'http://www4.hcdn.gob.ar/fotos/cuadradas/' + diputado.email + '.jpg';
 		  			legislators.push(diputado);
 		  		}
 		  		members.push({
 		  			commission: commission.id,
 		  			charge: integrante.cargo.id,
 		  			legislator: integrante.diputado.id
 		  		});
 		  	});
 		  });

 		  Commission.findOrCreate(commissions).exec(function (err, commissionsCreated) {
	 		  Block.findOrCreate(blocks).exec(function (err, records) {
	 		  	Legislator.findOrCreate(legislators).exec(function (err, legislatorsCreated) {
	 		  		Charge.findOrCreate(charges).exec(function(err, chargesCreated) {
	 		  			Member.findOrCreate(members).exec(function (err, membersCreated) {
	 		  				res.ok(commissionsCreated);
	 		  			})
	 		  		})
	 		  	});
	 		  });
 		  });
		});	
	},	
};

